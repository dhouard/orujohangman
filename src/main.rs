use std::fs::File;
use std::io::{BufReader, BufRead};
use rand::Rng;
use console::Term;
use regex::Regex;

fn load_dictionary() -> Vec<String>{
    let file_dict = File::open("dictionary.dat").expect("No se pudo abrir el fichero");
    let reader = BufReader::new(file_dict);
    let mut dictionary :Vec<String> = Vec::new();
    for line in reader.lines() {
        dictionary.push(line.unwrap());
    }
    
    dictionary
}

fn get_random_word(dictionary: Vec<String>) -> Vec<char> {
    let number = rand::thread_rng().gen_range(0..dictionary.len());

    dictionary[number].chars().collect()
}

fn draw_word(hits: Vec<char>) {
    println!();
    for i in 0..hits.len() {
        print!("{}", hits[i]);
    }
    println!("");
}

fn is_hit(letter: char, word: &Vec<char>) -> bool {
    if word.len() == 0 {
        return false;
    }
    for current in word {
        if *current == letter {
            return true;
        }
    }

    false
}

fn different_chars_count(word: Vec<char>) -> usize {
    let mut word_aux = word.clone();
    word_aux.sort();
    word_aux.dedup();

    word_aux.len()
}

fn fill_hits(hits: &mut Vec<char>, word :&Vec<char>, letter :char) {
    let mut index = 0;
    for current_char in word {
        if *current_char == letter {
            hits[index] = letter;
        }
        index += 1;
    }
}

fn draw_hangman(misses: usize) {
    println!(" ┏━━┳");
    print!(" ┃");
    if misses > 0 {
        print!("  o");
    }
    println!();
    print!(" ┃ ");
    if misses > 1 {
        print!("/");
    }
    if misses > 2 {
        print!("|");
    }
    if misses > 3 {
        print!("\\");
    }
    println!("");
    print!(" ┃ ");
    if misses > 4 {
        print!(" |");
    }
    println!();
    print!(" ┃");
    if misses > 5 {
        print!(" /");
    }
    if misses > 6 {
        print!(" \\");
    }
    println!();
    println!(" ┃");
    println!("┏┻━━━┓");
    println!("┃░░░░┃");
}

fn main() {
    let term = Term::stdout();
    term.clear_screen().unwrap();
    let dictionary = load_dictionary();
    let mut another :char;
    loop {
        let word = get_random_word(dictionary.clone());
        let mut hits  :Vec<char> = vec!['_'; word.len()];
        let mut hits_count :usize = 0;
        let mut misses :usize = 0;
        let number_goal = different_chars_count(word.clone());
        println!();
        let pattern = Regex::new("[a-zA-Z]").unwrap();
        while hits_count < number_goal && misses < 7 {
            draw_word(hits.clone());
            println!();
            println!("Intoduce una letra");
            let mut key :char;
            loop {
                key = Term::read_char(&term).unwrap();
                if pattern.is_match(key.to_string().as_str()) {
                    break;
                }
            }
            term.clear_screen().unwrap();
            if is_hit(key, &word) {
                println!("La letra {} se encuentra en la cadena", key);
                hits_count += 1;
                fill_hits(&mut hits, &word, key);
            } else {
                println!("La letra {} no se encuentra en la palabra", key);
                misses += 1;
            }
            draw_hangman(misses.clone());
        }
        let word_str = word.into_iter().collect::<String>();
        if misses > 6 {
            println!("Perdiste. La palabra era {}", word_str);
        } else {
            println!("Genial. La palabra era {}", word_str);
        }
        println!("¿Otra partida? (s/n)?");
        another = term.read_char().unwrap();
        term.clear_screen().unwrap();
        if another == 'n' || another == 'N' {
            break;
        }
    }
}
